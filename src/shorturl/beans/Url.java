package shorturl.beans;

import java.sql.Timestamp;

public class Url {

	private String urlLong;
	private String urlShort;
	private String password;
	private String date;
	private String dateTo;
	private String isActive;
	private Integer clicks;

	public String getUrlLong() {
		return urlLong;
	}

	public void setUrlLong(String urlLong) {
		this.urlLong = urlLong;
	}

	public String getUrlShort() {
		return urlShort;
	}

	public void setUrlShort(String urlShort) {
		this.urlShort = urlShort;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String date) {
        this.dateTo = date;
    }

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer click) {
        this.clicks = click;
    }

}
