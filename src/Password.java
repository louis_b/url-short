import Filters.Redirect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "PasswordCheck", urlPatterns = "/passwordCheck")
public class Password extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String password = request.getParameter("password");
        String passwordPrompt = request.getParameter("passwordPrompt");

        Connection conn = null;

        if (password.equalsIgnoreCase(passwordPrompt)) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                System.out.println(">> Driver connected");
                conn = DriverManager.getConnection("jdbc:mysql://0.0.0.0/shorturl", "root", "root");
                System.out.println(">> Connected");

                String anSQLquery = "SELECT * FROM urls WHERE id = ?";
                PreparedStatement state = conn.prepareStatement(anSQLquery);
                state.setString(1, request.getParameter("id"));
                ResultSet resultSet = state.executeQuery();

                while(resultSet.next()) {
                    String redirectUrl = resultSet.getString(2);

                    Redirect.updateClickValue(resultSet.getInt(1), resultSet.getInt(8));
                    response.sendRedirect(redirectUrl);
                    return;
                }

                conn.close();

            } catch (ClassNotFoundException e) {
                System.err.println(">> Driver not connected");
            } catch (SQLException e) {
                System.err.println("Erreur SQL : " + e.getMessage());
            }
        } else {
            HttpSession session = request.getSession(false);
            session.setAttribute("errorPassword", "Erreur dans le mot de passe! Veuillez réessayer");

            response.sendRedirect(request.getHeader("referer"));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
