package Filters;

import shorturl.beans.Url;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Redirect implements Filter {

    public void init( FilterConfig config ) throws ServletException {
        // ...
    }

    public void doFilter( ServletRequest req, ServletResponse res, FilterChain chain ) throws IOException,
            ServletException {
        /* Cast des objets request et response */
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();


        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println(">> Driver connected");
            Connection conn = DriverManager.getConnection("jdbc:mysql://0.0.0.0/shorturl", "root", "root");
            System.out.println(">> Connected");

            String currentUrl = request.getRequestURL().toString();
            String anSQLquery = "SELECT * FROM urls WHERE url_short = ?";
            PreparedStatement state = conn.prepareStatement(anSQLquery);
            state.setString(1, currentUrl);
            ResultSet resultSet = state.executeQuery();

            while(resultSet.next()) {
                Integer id = resultSet.getInt(1);
                String redirectUrl = resultSet.getString(2);
                Date today = java.sql.Date.valueOf(LocalDate.now());
                Date dateBefore = resultSet.getDate(6);
                Date dateAfter = resultSet.getDate(7);
                Integer click = resultSet.getInt(8);

                // if date null
                if (dateBefore == null || dateAfter == null) {
                    // test if password
                    if (resultSet.getString(4) != null) {
                        request.setAttribute("password", resultSet.getString(4));
                        request.setAttribute("id", resultSet.getInt(1));

                        request.getServletContext().getRequestDispatcher("/WEB-INF/password.jsp").forward(request, response);
                        return;
                    }

                    updateClickValue(id, click);
                    response.sendRedirect(redirectUrl);
                    return;
                }

                // test if correspond to date interval
                if ((today.after(dateBefore) || today.equals(dateBefore)) && (today.before(dateAfter) || today.equals(dateAfter))) {

                    // test if password
                    if (resultSet.getString(4) != null) {
                        request.setAttribute("password", resultSet.getString(4));
                        request.setAttribute("id", resultSet.getInt(1));

                        request.getServletContext().getRequestDispatcher("/WEB-INF/password.jsp").forward(request, response);
                        return;
                    }

                    this.updateClickValue(id, click);
                    response.sendRedirect(redirectUrl);
                    return;
                } else {
                    request.getServletContext().getRequestDispatcher("/WEB-INF/invalidDate.jsp").forward(request, response);
                    return;
                }
            }

            conn.close();

        } catch (ClassNotFoundException e) {
            System.err.println(">> Driver not connected");
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e.getMessage());
        }
    }

    public void destroy() {
        // ...
    }

    public static void updateClickValue(Integer id, Integer nbClicks) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://0.0.0.0/shorturl", "root", "root");

            String sql = "UPDATE urls set nbClics = ? WHERE id = ?";
            PreparedStatement state = conn.prepareStatement(sql);
            state.setInt(1, nbClicks + 1);
            state.setInt(2, id);
            state.executeUpdate();

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
