<%@ include file="header.jsp"%>
<div class="container">
    <blockquote>Entrer le mot de passe pour accéder à l'url</blockquote>
    <div class="row">
        <form class="col s12" action="/passwordCheck" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <input id="passwordPrompt" type="password" name="passwordPrompt"> <label
                        for="passwordPrompt">Password</label>
                    <input type="hidden" name="password" value="${password}">
                    <input type="hidden" name="id" value="${id}">
                </div>
            </div>
            <button class="btn waves-effect waves-light" type="submit"
                    name="action">
                Submit <i class="material-icons right">send</i>
            </button>
            <% if (session.getAttribute("errorPassword") != null) { %>
            <br><br>
                <div class="card-panel red lighten-1">
                    <span class="white-text">
                        <%
                            out.println(session.getAttribute("errorPassword"));
                            session.removeAttribute("errorPassword");
                        %>
                    </span>
                </div>
            <% } %>
        </form>
    </div>
</div>
<%@ include file="footer.jsp"%>