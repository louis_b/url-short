<%@ include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="shorturl.beans.Url"%>
<div class="container">
	<div class="row">
		<div class="white-text center-align card-panel teal lighten-2">
			<h5>Mes urls</h5>
		</div>
	</div>
	<div class="row">
        <a class="waves-effect waves-light btn" href="/admin/urls"><i class="material-icons left">refresh</i>Refresh</a>
        <table class="striped">
			<thead>
				<tr>
					<th>Short Url</th>
					<th>Long Url</th>
					<th>From</th>
					<th>To</th>
					<th>Click</th>
				</tr>
			</thead>
			<tbody>
			<%
				List<Url> urls = (List<Url>) request.getAttribute("listUrl");
				if (urls != null) {
					for (Url url : urls) {
			%>
			<tr>
			<td><a href="<%=url.getUrlShort()%>" target="_blank"><%=url.getUrlShort()%></a></td>
			<td><a href="<%=url.getUrlLong()%>" target="_blank"><%=url.getUrlLong()%></a></td>
			<td><%=url.getDate() == null ? "Non définie" : url.getDate()%></td>
			<td><%=url.getDateTo() == null ? "Non définie" : url.getDateTo()%></td>
			<td><%=url.getClicks()%></td>
			</tr>
			<%
				}
				}
			%>
			</tbody>
		</table>
	</div>
</div>
<%@ include file="footer.jsp"%>